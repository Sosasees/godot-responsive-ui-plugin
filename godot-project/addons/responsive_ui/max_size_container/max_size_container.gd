tool
extends Container

enum Align {ALIGN_BEGIN, ALIGN_CENTER, ALIGN_END}
export var max_size : Vector2 = Vector2(-1, -1)
export(Align) var h_alignment : int = Align.ALIGN_CENTER
export(Align) var v_alignment : int = Align.ALIGN_CENTER
onready var _child : Control = get_child(0)


func _process(_delta) -> void:
	if max_size.x < 0:
		_child.rect_size.x = get_size().x
	else:
		_child.rect_size.x = min(get_size().x, max_size.x)
	
	if max_size.y < 0:
		_child.rect_size.y = get_size().y
	else:
		_child.rect_size.y = min(get_size().y, max_size.y)
	
	if max_size.x < 0:
		_child.rect_position.x = 0
	else:
		if _child.get_size().x < get_size().x:
			if h_alignment == Align.ALIGN_END:
				_child.rect_position.x = get_size().x - _child.get_size().x
			elif h_alignment == Align.ALIGN_CENTER:
				_child.rect_position.x = (
						get_size().x - _child.get_size().x
				) / 2
			elif h_alignment == Align.ALIGN_BEGIN:
				_child.rect_position.x = 0
	
	if max_size.y < 0:
		_child.rect_position.y = 0
	else:
		if _child.get_size().y < get_size().y:
			if v_alignment == Align.ALIGN_END:
				_child.rect_position.y = get_size().y - _child.get_size().y
			elif v_alignment == Align.ALIGN_CENTER:
				_child.rect_position.y = (
						get_size().y - _child.get_size().y
				) / 2
			elif v_alignment == Align.ALIGN_BEGIN:
				_child.rect_position.y = 0
