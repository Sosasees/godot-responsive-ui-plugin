extends Control


var baseline_dpi : int = 72


func _ready():
	
	match OS.get_name():
		"Android":
			baseline_dpi = 160
		"X11":
			baseline_dpi = 96
		_:
			baseline_dpi = 72
	
	set_anchor(MARGIN_TOP, 0, true)
	set_anchor(MARGIN_RIGHT, 0, true)
	set_anchor(MARGIN_BOTTOM, 0, true)
	set_anchor(MARGIN_LEFT, 0, true)


func _process(_delta):
	set_position(
		OS.get_window_safe_area().position
	)
	
	set_size(
		OS.get_window_safe_area().size / OS.get_screen_dpi() * baseline_dpi
	)
	
	set_scale(
		Vector2.ONE / baseline_dpi * OS.get_screen_dpi(-1)
	)
