tool
extends EditorPlugin


func _enter_tree():
	add_custom_type(
			"ResponsiveBaseContainer",
			"Control",
			preload("responsive_base_container/responsive_base_container.gd"),
			preload("responsive_base_container/responsive_base_container.svg")
	)
	
	add_custom_type(
			"WidthBreakpointContainer",
			"Control",
			preload("breakpoint_container/width_breakpoint_container.gd"),
			preload("breakpoint_container/width_breakpoint_container.svg")
	)
	add_custom_type(
			"HeightBreakpointContainer",
			"Control",
			preload("breakpoint_container/height_breakpoint_container.gd"),
			preload("breakpoint_container/height_breakpoint_container.svg")
	)
	add_custom_type(
			"OrientationBreakpointContainer",
			"Control",
			preload("breakpoint_container/orientation_breakpoint_container.gd"),
			preload("breakpoint_container/orientation_breakpoint_container.svg")
	)
	
	add_custom_type(
			"MaxSizeContainer",
			"Container",
			preload("max_size_container/max_size_container.gd"),
			preload("max_size_container/max_size_container.svg")
	)


func _exit_tree():
	remove_custom_type("ResponsiveBaseContainer")
	
	remove_custom_type("WidthBreakpointContainer")
	remove_custom_type("HeightBreakpointContainer")
	remove_custom_type("OrientationBreakpointContainer")
	
	remove_custom_type("MaxSizeContainer")
