extends Control


export var allow_square : bool = true
export var allow_landscape : bool = true
export var allow_portrait : bool = true


func _process(_delta) -> void:
	if get_parent().get_size().x == get_parent().get_size().y:
		set_visible(allow_square)
	elif get_parent().get_size().x > get_parent().get_size().y:
		set_visible(allow_landscape)
	elif get_parent().get_size().x < get_parent().get_size().y:
		set_visible(allow_portrait)
