extends Control


export var min_height : int = 0
export var max_height : int = 65535


func _process(_delta) -> void:
	if get_parent().get_size().x >= min_height and \
			get_parent().get_size().x <= max_height:
		set_visible(true)
	else:
		set_visible(false)
