extends Control


export var min_width : int = 0
export var max_width : int = 65535


func _process(_delta) -> void:
	if get_parent().get_size().x >= min_width and \
			get_parent().get_size().x <= max_width:
		set_visible(true)
	else:
		set_visible(false)
