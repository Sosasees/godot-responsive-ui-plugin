⚠️ i discontinued this Godot 3 edition of the add-on
because it's obseleted by the [Godot 4 edition](https://codeberg.org/sosasees/godot-4-responsive-ui-plugin).

# responsive UI plugin for Godot

this Godot add-on is a collection of nodes
for making the creation of responsive layouts easier.
it works in [Godot](https://godotengine.org) ``v3.4.4.stable.official [419e713a2]``.

[▶️ Try Live Demo](https://gotm.io/sosasees-demos/responsive-ui-plugin/)

## table of contents

- how to install
- nodes that this add-on adds

## how to install

### 1. download and unpack

1. go to the [_Releases_ tab](https://codeberg.org/Sosasees/godot-responsive-ui-plugin/releases) on the repository page
2. look for the version you want to install in the list.
   i recommend the first _stbale_ release that shows up
   because it's the newest _stable_ version
3. in the section of your chosen release,
   click the first item below _Source Code (TAR.GZ)_
   to download the add-on folder in ZIP format
4. unpack the downloaded ZIP folder

### 2. install

1. move the unpacked folder into the ``res://addons/`` folder of your Godot project.
   the final add-on path looks something like ``res://addons/responsive_ui/``
2. enable the plug-in under _Project > Project Settings… > Plugins_:
   * activate the checkbox on the line where you find the name _ResponsiveUI_

## nodes that this add-on adds

### responsive base container

* Node: Control/**ResponsiveBaseContainer**
  * rescales itself based on the current screen's density
  * moves itself inside the safe area,
    so that it won't overlap the notch on Android phones
  * ⚠ leave the project setting _Display > Window > Stretch > Mode_ as _disabled_
    before using this node, to avoid unexpected behavior

### breakpoint containers

* Node: Control/**WidthBreakpointContainer**
  * makes itself visible only if the parent _Control_ is
    * wider or equal than ``min_width``
    * shorter or equal than ``max_width``
  * ⚠ must be child of a _Control_ node to avoid errors

* Node: Control/**HeightBreakpointContainer**
  * makes itself visible only if the parent _Control_ is
    * taller or equal than ``min_height``
    * shorter or equal than ``max_height``
  * ⚠ must be child of a _Control_ node to avoid errors

* Node: Control/**OrientationBreakpointContainer**
  * makes itself visible only if the parent _Control_ has one of the orientations that are set as allowed
    * the options are _Square_ & _Landscape_ & _Portrait_
  * ⚠ must be child of a _Control_ node to avoid errors

### max size container

* Node: Control/Container/**MaxSizeContainer**
  * restricts the size of the child Control to a specified ``max_size``
  * ⚠ only 1 child is supported.
      adding more children causes unexpected behavior
  * ⚠ in these situations you will have to restart current project before you can see live editor preview of MaxSizeContainer's effects:
    * after you added the child Control
    * after you changed the child Control to a different node or type
